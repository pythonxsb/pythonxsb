\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage[margin=0.5in]{geometry}
\usepackage{listings}
\usepackage[square,numbers]{natbib}
\bibliographystyle{unsrtnat}
\title{ PX - Python Interface \\ User Manual \\ Version 1.0}
\author{ Muthukumar Suresh \\ C.R. Ramakrishnan}
\begin{document}
\date{}
\maketitle
\newpage
\tableofcontents
\newpage
\section{Introduction}
The PX-Python is the first of its kind interface between Prolog and Python that can be initiated from Prolog. Previous work has been done between Java and Prolog in \cite{11}, \cite{12}, \cite{13}. There are similar interfaces for Python and Prolog such as PyLog\cite{14} which provide Prolog support for python. While some of these like GNU Prolog are uni-directional, there are others like Interprolog which provide Bi-directional support for Java and Prolog. But all these interfaces are common in the sense that Prolog is being called from Java and Python. So for the new language PX that we are building on top of XSB Prolog, we cannot use these existing interfaces. \\
Hence, we have built our own interface which can be initiated from Prolog and can leverage the numerous Python libraries like Scikit, NumPy amongst others. The possibilities of this interface is endless and we hope that logic programmers can come up with new and creative uses for this interface. We ourselves have used this interface to use Scikit libraries in PX for machine learning libraries that do not have the corresponding implementation in Prolog.\\
This manual highlights the various APIs offered by this interface for calling functions in python modules, dealing with python objects and examples on how to use them.

\section{System Requirements}
The current interface has been tested on  unix and its varients(Linux and Mac) and works well on those operating systems. While  Windows is not supported currently, we hope to add support for it in the future. 
\section{License}
The PX-Python interface is distributed under the GNU LIBRARY GENERAL PUBLIC LICENSE.

\section{Installation and Running}
Configuring the interface is simple using the following steps:
\begin{enumerate}
\item In configure.ac, change the following lines:
\begin{verbatim}
pythoninclude="PATH_TO_PYTHON2.7_INCLUDE_FOLDER" usually "/usr/include/python-2.7"
xsbinclude="$XSB_PATH/XSB/XSB-*.*/emu" 
\end{verbatim}
\item run the following commands in the terminal:
\begin{verbatim}
 $ autoconf
 $ ./configure
\end{verbatim} 
\end{enumerate}
To use the interface from xsb:

\begin{verbatim}
    $ xsb
    :- [ld_pymod].
\end{verbatim}

To test that the interface is working as it should : 
\begin{verbatim}
?- ['test/testSuite'].
?- beginTest.
\end{verbatim}
See that all tests pass successfully. 
\section{Data Types} \label{datatype}
The interface supports all basic types like :
\begin{enumerate}
\item Integer (long)
\item float (double)
\item string
\end{enumerate}
Handling of the following complex types depends on how python objects can be represented in prolog and vise-versa. Below we highlight, how we handle the complex types between python and prolog. 
\begin{enumerate}
\item Python Objects are represented in Prolog as pyObject(value). The value acts as the identifier for a python object. Python Objects have no real purpose on the Prolog side but they can be used as parameters to a python function. E.g., consider a python dictionary module that has a init\_dict() function that returns a python dictionary object. To add elements to a dictionary, we could pass this pyObject and the key-value to be added to a add\_dict(DictObject, Key, Value) function. 
\item Python Iterators are represented in Prolog as pyIterator(value). The value acts as the identifier for a python iterator. Python Iterators can be passed to the predicate pyObj\_Next/2 to get the next element pointed to by the iterator as shown in section \ref{nextlist}. 
\item Python Tuples are represented in Prolog as a term of arity 1 with the functor symbol pyTuple and a value that stores the reference to the python tuple object. We can use pyObj_GetIter and pyObj_Next to get the elements of the tuple.  
\item Lists in Python are represented in Prolog as terms of arity 1 with the functor symbol pyList. The 3 predicates explained in section \ref{nextlist}, \ref{iter} and \ref{convlist} can be used to deal with lists or any iterable objects in python.
\end{enumerate}


\section{callpy/3} \label{callpy}
This predicate calls a function present in any python module and unifies argument 3 with a list, tuple, int, float, string or pyobject. If the python function does not return a value, argument 3 is not unified with a value. \\
Argument Specification:
\begin{itemize}
\item \textbf{Argument 1} : specifies the python module that needs to be loaded. The python module can be specified as an absolute or relative path.
\item \textbf{Argument 2} : is of the form function\_name(Arg1, Arg2, Arg3, ..). Argument 2 specifies the python function that has to be called. The arguments can be any of the data types specified in section \ref{datatype}.  
\item \textbf{Argument 3} : is a Prolog variable that will get unified with the result from the python function call if the function returns a value. The return value can be any of the data types specified in section \ref{datatype}.
\end{itemize}
Examples:\\
\begin{verbatim}
Consider the following python program that creates, adds and gets elements from a dictionary :

#Module Name : examples/dict_text.py
def init_dict():
    dictionary = {}
    return dictionary
    
def add_dict(dictionary, key, value):
    if type(dictionary) == type(dict()):
	    dictionary[key] = value
        return 1
    else:
        return 0

def get_value(dictionary, key):
    if key in dictionary.keys():
        return dictionary[key]
\end{verbatim}
To call this python module from Prolog :
\begin{verbatim}
:- [ld_pymod].
:- callpy(`examples/dict_test', init_dict(), X), assert(keyval(X, dict1)).
    X = pyObject(p0xDEADBEEF).
:- keyval(X, dict1), callpy(dict_test, add_dict(X, `hello', 42), Z).
    X = pyObject(p0xDEADBEEF).
    Z = 1
:- keyval(X,dict1), callpy(dict_test, get_value(X, `hello'),Z).
    Z = 42.
\end{verbatim}

\section{pyObj\_GetIter/2} \label{iter}
This predicate when called with a pyObject/1 or pyList/1 or pyTuple/1 unifies argument 2 with a pyIterator/1 python iterator, if possible. It can be used to get 1 or more iterators for the python object if the python object is iterable, otherwise, it returns false. 
Argument Specification : 
\begin{enumerate}
\item \textbf{Argument 1} : pyList/1 that corresponds to a python object.
\item \textbf{Argument 2} : returns a pyIterator/1 that corresponds to a python iterator object. 
\end{enumerate}
Example :
Consider the python program that adds a integer value to all elements of a list : 
\begin{verbatim}
#Module Name : examples/sumlist3.py
def sumlist3(X,Y):
    Z = []
    for element in Y:
	    Z.append(X + element)
    return Z
\end{verbatim}
From Prolog :
\begin{verbatim}
:- callpy(`examples/sumlist3', sumlist3(5,[1,2,3]),Z), assert(keyval(Z, list)).
    Z = pyList(p0xDEADBEEF).
:- keyval(Z, list), pyObj_GetIter(Z, Iter), assert(keyval(Iter, iter1)).
    Z = pyObject(p0xDEADBEEF).
    Iter = pyIterator(p0xDEADF00D).
\end{verbatim}
We will explain the useage of iterator objects in section \ref{nextlist}.

\section{pyObj\_Next/2} \label{nextlist}
This predicate when called with a pyIterator/1 unifies argument 2 with the element pointed by the iterator and the iterator goes to the next element. If the iterator has reached the end of the iterable object, it returns false. 
Argument Specification:
\begin{enumerate}
\item \textbf{Argument 1} : a pyIterator/1 that corrosponds to a iterator object.
\item \textbf{Argument 2} : a value which is one of the types specified in section \ref{datatype}.
\end{enumerate}
Example : 
Continuing from section \ref{iter}, 
From Prolog:
\begin{verbatim}
:- keyval(Z, iter), pyObj_next(Z,Val).
    Z  = pyIterator(p0xDEADF00D).
    Val = 6.
\end{verbatim}
\section{pyList2prList/2} \label{convlist}
This predicate is used to convert a python list object as represented by pyObject/1 into a Prolog list. Note: it recursively converts all python Lists inside into prolog lists too. It however leaves tuples as pyTuple/1 and its elements can only be accessed using pyObj\_GetIter and pyObj\_Next. 
Argument Specification:
\begin{enumerate}
\item Argument 1 : corresponds to a pyList/1 that represents a python list object. 
\item Argument 2 : result variable which is unified with a prolog list object.
\end{enumerate}
Continuing from section \ref{iter}, 
From Prolog:
\begin{verbatim}
:- keyval(Z, list), pyList2prList(Z,Val).
    Z  = pyObject(p0xDEADF00D).
    Val = [6,7,8].
\end{verbatim}
\section{Querying XSB from Python}
This interface also allows Python to query XSB using the querySingle function in xsbp module. 
Argument Specification: 
\begin{enumerate}
\item The function takes 1 argument which is the query we want to run in XSB.
\item The function returns a python List which has all the values assigned to the variables in the query. The order of the values in the result list is the same as that in the query. Note: only the first response is returned. Currently, there is no mechanism to get multiple answers back. 
\end{enumerate}
Example : \\
Consider that prolog has these facts :
\begin{verbatim}
p(1).
p(2).
q(3).
\end{verbatim}
We also have a python file testc.py:
\begin{verbatim}
import xsbp
def tester():
	d = xsbp.querySingle("p(X), q(Z), p(X).")
	return d
\end{verbatim}
When we call from XSB:
\begin{verbatim}
?- callpy('testc', tester(), X).
yes
X = [1,3];
no
\end{verbatim}
\medskip
\bibliography{sample}
\end{document}


