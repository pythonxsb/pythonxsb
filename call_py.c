//author: Muthukumar Suresh muthukumar5393@gmail.com
#include<python2.7/Python.h>
#include <cinterf.h>
#include <context.h>
#include "xsbpmodule.c"
//region : function decleration(s)
int convert_pyObj_prObj(CTXTdeclc PyObject *, prolog_term *, int);
int convert_prObj_pyObj(CTXTdeclc prolog_term , PyObject **);
//endregion

enum prolog_term_type 
{
	INT = 0,
	FLOAT = 1,
	STRING = 2,
	LIST = 3,
	NIL = 4,
	FUNCTOR = 5,
	VAR = 6,
	REF = 7,
	ITER = 8,
	TUP = 9,
	PYLIST = 10, 
};

int is_reference(prolog_term term)
{
	char *result = p2c_string(term);
	if(strncmp("ref_", result, 4)== 0 ){
		return 1;
	}
	return 0;
}

int find_prolog_term_type(CTXTdeclc prolog_term term)
{
	if(is_float(term))
		return FLOAT;
	else if(is_int(term))
		return INT;
	else if(is_list(term))
		return LIST;
	else if(is_var(term))
		return VAR;
	else if(is_nil(term))
		return NIL;
    else if(is_string(term)){
        return STRING;
    }
	else if(is_functor(term))
	{
		if(strcmp(p2c_functor(term),"pyObject") == 0 )
		{
			return REF;	
		}
		else if(strcmp(p2c_functor(term),"pyList") == 0)
			return PYLIST;
		else if(strcmp(p2c_functor(term),"pyIterator") == 0)
			return ITER;
		else if(strcmp(p2c_functor(term), "pyTuple") == 0)
			return TUP;
		return FUNCTOR;
	}
	else 
		return -1;
}

int find_length_prolog_list(prolog_term V)
{
	prolog_term temp = V;
	int count= 0;
	while(!(is_nil(temp)))
	{
		p2p_car(temp);
		count++;
		temp = p2p_cdr(temp);
	}
	return count;
}

int prlist2pyList(CTXTdeclc prolog_term V, PyObject *pList, int count)
{
	prolog_term temp = V;
	prolog_term head;
	int i;
	for(i = 0; i <count;i++)
	{ 
		head = p2p_car(temp);
		PyObject *pyObj = NULL;
		if( !convert_prObj_pyObj(CTXTc head, &pyObj))
			return FALSE;
		PyList_SetItem(pList, i, pyObj);
		temp = p2p_cdr(temp);
	}	
	return TRUE;
}
prolog_term convert_pyTuple_prTuple(CTXTdeclc PyObject *pyObj){
	size_t size = PyTuple_Size(pyObj);
	
	prolog_term P = p2p_new(CTXT);
	if(size >=2){
		c2p_functor(CTXTc ",", 2, P);
		prolog_term temp = P;
		int i = 1;
		while(i <size-1)
		{
			PyObject *pyObjinner = PyTuple_GetItem(pyObj, i-1);
        	prolog_term ithterm = p2p_arg(temp, 1);
        	convert_pyObj_prObj(CTXTc pyObjinner, &ithterm, 0);
        	temp = p2p_arg(temp,2);
        	c2p_functor(CTXTc ",", 2, temp);
        	i++;
		}
		PyObject *pyObjinner = PyTuple_GetItem(pyObj, i-1);
    	prolog_term ithterm = p2p_arg(temp, 1);
    	convert_pyObj_prObj(CTXTc pyObjinner, &ithterm, 0);
    	ithterm = p2p_arg(temp, 2);
    	pyObjinner = PyTuple_GetItem(pyObj, i);
    	convert_pyObj_prObj(CTXTc pyObjinner, &ithterm, 0);
	}
	else
	{
		c2p_functor(CTXTc ",", size, P);
		if(size == 1){
			PyObject *pyObjinner = PyTuple_GetItem(pyObj, 0);
    		prolog_term ithterm = p2p_arg(P, 1);
    		convert_pyObj_prObj(CTXTc pyObjinner, &ithterm, 0);
		}
	}
	return P;
}
int convert_pyObj_prObj(CTXTdeclc PyObject *pyObj, prolog_term *prTerm, int flag)
{
	if(pyObj == Py_None){
		return 1;// todo: check this case for a list with a none in the list. how does prolog side react 
	}
	if(PyInt_Check(pyObj))
	{
		prolog_int result = PyInt_AS_LONG(pyObj);
		c2p_int(CTXTc result, *prTerm);
		return 1;
	}
	else if(PyFloat_Check(pyObj))
	{
		float result = PyFloat_AS_DOUBLE(pyObj);
		c2p_float(CTXTc result, *prTerm);
		return 1;
	}else if(PyString_Check(pyObj))
	{
		char *result = PyString_AS_STRING(pyObj);
		c2p_string(CTXTc result, *prTerm);
		return 1;
    }
    else if(PyTuple_Check(pyObj))
    {
        // size_t  i;
        // size_t size = PyTuple_Size(pyObj);
        // prolog_term P = p2p_new();
        // c2p_functor("",size,P);
        // for (i = 0; i < size; i++)
        // {
        //     PyObject *pyObjinner = PyTuple_GetItem(pyObj, i);
        //     prolog_term ithterm = p2p_arg(P, i+1);
        //     convert_pyObj_prObj(pyObjinner, &ithterm);
        // }
        // prolog_term P = convert_pyTuple_prTuple(CTXTc pyObj);
        // if(!p2p_unify(CTXTc P, *prTerm))
        	// return FALSE;
    	char str[30];
		sprintf(str, "p%p", pyObj);
	  	prolog_term ref = p2p_new(CTXT);
		c2p_functor(CTXTc "pyTuple", 1, ref);
		prolog_term ref_inner = p2p_arg(ref, 1);
    	c2p_string(CTXTc str, ref_inner);		
		if(!p2p_unify(CTXTc ref, *prTerm))
			return FALSE;	
		return TRUE;
    }
    else if(flag == 0 && PyList_Check(pyObj))
    {
    	char str[30];
		sprintf(str, "p%p", pyObj);
	  	prolog_term ref = p2p_new(CTXT);
		c2p_functor(CTXTc "pyList", 1, ref);
		prolog_term ref_inner = p2p_arg(ref, 1);
    	c2p_string(CTXTc str, ref_inner);		
		if(!p2p_unify(CTXTc ref, *prTerm))
			return FALSE;	
		return TRUE;
    }
	else
	{	
		if(flag == 1 && PyList_Check(pyObj))
		{
			size_t size = PyList_Size(pyObj);
			size_t i = 0;
			prolog_term head, tail;
			prolog_term P = p2p_new(CTXT);
			tail = P;
		
			for(i = 0; i < size; i++)
			{
				c2p_list(CTXTc tail);
				head = p2p_car(tail);
				PyObject *pyObjInner = PyList_GetItem(pyObj, i);
				convert_pyObj_prObj(CTXTc pyObjInner, &head, 1);				
				tail = p2p_cdr(tail);
			}
			c2p_nil(CTXTc tail);
			if(!p2p_unify(CTXTc P, *prTerm))
				return FALSE;
			return TRUE;
		}
		char str[30];
		sprintf(str, "p%p", pyObj);
	  	prolog_term ref = p2p_new(CTXT);
		c2p_functor(CTXTc "pyObject", 1, ref);
		prolog_term ref_inner = p2p_arg(ref, 1);
    	c2p_string(CTXTc str, ref_inner);		
		if(!p2p_unify(CTXTc ref, *prTerm))
			return FALSE;	
		return TRUE;
	}
}
int convert_prObj_pyObj(CTXTdeclc prolog_term prTerm, PyObject **pyObj)
{
	if(find_prolog_term_type(CTXTc prTerm) == INT)
	{
		prolog_term argument = prTerm;
		prolog_int argument_int = p2c_int(argument);
		*pyObj = PyInt_FromLong(argument_int);
		return TRUE;
	}else if(find_prolog_term_type(CTXTc prTerm) == STRING)
	{
		prolog_term argument = prTerm;
		char *argument_char = p2c_string(argument);
		*pyObj = PyString_FromString(argument_char);
		return TRUE;
	}else if(find_prolog_term_type(CTXTc prTerm) == FLOAT)
	{
		prolog_term argument = prTerm;
		prolog_float argument_float = p2c_float(argument);
		*pyObj = PyFloat_FromDouble(argument_float);
		return TRUE;
	}
	else if(find_prolog_term_type(CTXTc prTerm) == LIST || find_prolog_term_type(CTXTc prTerm) == NIL )
	{

		prolog_term argument = prTerm;
			int count = find_length_prolog_list(argument);
			PyObject *pList = PyList_New(count);
			if(!prlist2pyList(CTXTc argument, pList, count))
				return FALSE;
			*pyObj = pList;
			return TRUE;
	}
	
	else if (find_prolog_term_type(CTXTc prTerm) == REF)
	{
			prolog_term ref = p2p_arg(prTerm, 1);
			char *node_pointer = p2c_string(ref); 
			PyObject *pyobj_ref = (PyObject *)strtol(node_pointer+1,NULL, 0);
			*pyObj = pyobj_ref;
			return TRUE;
	}
	return FALSE;
}
int set_python_argument(CTXTdeclc prolog_term temp, PyObject *pArgs,int i)
{
	PyObject *pValue;
	if(!convert_prObj_pyObj(CTXTc temp, &pValue))
		return FALSE;
	PyTuple_SetItem(pArgs, i-1, pValue);
	return TRUE;
}

char *set_path_name(char *module)
{
  char *directory_end = strrchr(module, '/');
	
	if(directory_end == NULL)
		return module;
	size_t directory_len = (size_t)(directory_end - module);//no need for last '/'
	// char *directory = malloc(strlen(getenv("PYTHONPATH")) + directory_len+2);
	// memset(directory, '\0',strlen(getenv("PYTHONPATH")) + directory_len+2);
	// strncpy(directory,getenv("PYTHONPATH"), strlen(getenv("PYTHONPATH")));
	// strncpy(directory+strlen(getenv("PYTHONPATH")), ":", 1);
	// strncpy(directory+strlen(getenv("PYTHONPATH"))+1,module, directory_len);
	// setenv("PYTHONPATH", directory,1);
	char *directory = malloc(directory_len+1);
	memset(directory, '\0',directory_len+1);
	strncpy(directory,module, directory_len);
	PyObject* sysPath = PySys_GetObject((char*)"path");
	PyObject* programName = PyString_FromString(directory);
	PyList_Append(sysPath, programName);
	Py_DECREF(programName);
	free(directory);
	module = (module + directory_len + 1);
	return module;
}
static PyMethodDef XsbMethods[] = {
    {"querySingle",  xsbp_querySingle, METH_VARARGS,
     "Query XSB from Python which returns the first response."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initasynccall(void)
{
    (void) Py_InitModule("xsbp", XsbMethods);
}

//todo: need to refactor this code.
int callpy(CTXTdecl)
{
	// if(strlen(getenv("PYTHONPATH")) == 0)
		setenv("PYTHONPATH", ".", 1);
	prolog_term mod = extern_reg_term(1);
	if(!Py_IsInitialized())
		Py_Initialize();	
	char *module = p2c_string(mod);
	module = set_path_name(module);
	PyObject *pName = NULL, *pModule = NULL, *pFunc = NULL;

	PyObject *pArgs = NULL, *pValue = NULL;
	prolog_term V, temp;
	
		
	initasynccall();
	pName = PyString_FromString(module);
	pModule = PyImport_ImportModule(module);
	if(pModule == NULL)
	{
		return FALSE;	
	}
	Py_DECREF(pName);
	V = extern_reg_term(2);
	char *function = p2c_functor(V);
	if(is_functor(V))
	{
		int args_count = p2c_arity(V);
		pFunc = PyObject_GetAttrString(pModule, function);
		Py_DECREF(pModule);
		if(pFunc && PyCallable_Check(pFunc))
		{
			pArgs = PyTuple_New(args_count);
			int i;
			for(i = 1; i <= args_count; i++)
			{
				temp = p2p_arg(V, i);
				if(!(set_python_argument(CTXTc temp, pArgs, i)))
				{
					return FALSE;
				}
			}
		}
		else
		{
			return FALSE;
		}
		pValue = PyObject_CallObject(pFunc, pArgs);
		prolog_term return_pr = p2p_new(CTXT);
		if(!convert_pyObj_prObj(CTXTc pValue, &return_pr, 0))
			return FALSE;
		if(!p2p_unify(CTXTc return_pr, reg_term(CTXTc 3)))
			return FALSE;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	Py_Finalize();
		return TRUE;
}
int pyObj_GetIter(CTXTdecl)
{
	prolog_term prTerm = extern_reg_term(1);
	if(find_prolog_term_type(CTXTc prTerm) == REF 
		|| find_prolog_term_type(CTXTc prTerm) == TUP 
		|| find_prolog_term_type(CTXTc prTerm) == PYLIST)
	{
		prolog_term ref_pyobj = p2p_arg(prTerm, 1);
		char *node_pointer = p2c_string(ref_pyobj); 
		PyObject *pValue = (PyObject *)strtol(node_pointer+1,NULL, 0);
		PyObject *iterator; 
		iterator = PyObject_GetIter(pValue);
		if(iterator == NULL)
			return FALSE;
		char str[30];
		sprintf(str, "p%p", iterator);
	  	prolog_term ref = p2p_new(CTXT);
		c2p_functor(CTXTc "pyIterator", 1, ref);
		prolog_term ref_inner = p2p_arg(ref, 1);
    	c2p_string(CTXTc str, ref_inner);		
		if(!p2p_unify(CTXTc ref, reg_term(CTXTc 2)))
			return FALSE;
		return TRUE;
	} 
	return FALSE;
}

int pyObj_Next(CTXTdecl)
{
	prolog_term prTerm = extern_reg_term(1);
	if(find_prolog_term_type(CTXTc prTerm) == ITER)
	{
		prolog_term ref_pyobj = p2p_arg(prTerm, 1);
		char *node_pointer = p2c_string(ref_pyobj); 
		PyObject *iterator = (PyObject *)strtol(node_pointer+1,NULL, 0);
		PyObject *obj = PyIter_Next(iterator);
		if(obj == NULL)
			return FALSE;
		prolog_term return_pr = p2p_new(CTXT);
		if(!convert_pyObj_prObj(CTXTc obj, &return_pr, 0))
			return FALSE;
		prolog_term prTerm = extern_reg_term(2);
		if(!p2p_unify(CTXTc return_pr, prTerm))
			return FALSE;
		return TRUE;
	} 
	return FALSE;
}

int pyList2prList(CTXTdecl)
{
	prolog_term prTerm = extern_reg_term(1);
	if(find_prolog_term_type(CTXTc prTerm) == PYLIST)
	{ 
		prolog_term ref = p2p_arg(prTerm, 1);
		char *node_pointer = NULL;
		node_pointer = p2c_string(ref);
		PyObject *pyobj_ref = (PyObject *)strtol(node_pointer+1,NULL, 0);
		if ( pyobj_ref == NULL)
			return FALSE;
		PyObject * pyObj = pyobj_ref;
		if (pyObj == NULL)
			return FALSE;
		if(PyList_Check(pyObj))
		{
			size_t size = PyList_Size(pyObj);
			size_t i = 0;
			prolog_term head, tail;
			prolog_term P = p2p_new(CTXT);
			tail = P;
		
			for(i = 0; i < size; i++)
			{
				c2p_list(CTXTc tail);
				head = p2p_car(tail);
				PyObject *pyObjInner = PyList_GetItem(pyObj, i);
				convert_pyObj_prObj(CTXTc pyObjInner, &head, 1);				
				tail = p2p_cdr(tail);
			}
			c2p_nil(CTXTc tail);
			prolog_term prTerm = extern_reg_term(2);
			if(!p2p_unify(CTXTc P, prTerm))
				return FALSE;
			return TRUE;
		}		
	}
	return FALSE;
}
