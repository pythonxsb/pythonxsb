
void build_result(char *res, PyObject **lis)
{
	char * pch;
	pch = strtok (res, "|");
	int counter = 1;
	while(pch!= NULL){
		PyList_Append(*lis, PyString_FromString(pch));
		pch = strtok (NULL, "|");
		
	}
}
static PyObject *xsbp_querySingle(PyObject *self, PyObject *args)
{
	char *cmd;
	if(!PyArg_ParseTuple(args, "s", &cmd))
		return NULL;
	int rcp;
	XSB_StrDefine(p_return_string);
	PyObject* resultList = PyList_New(0);
	xsb_query_save(3);
	rcp = xsb_query_string_string(cmd,&p_return_string,"|");
	xsb_close_query();
	xsb_query_restore();
	PyObject *lis = PyList_New(0);
	if(rcp == XSB_SUCCESS){
	build_result(p_return_string.string, & lis);
	PyList_Append(resultList,lis);
	}
	return resultList;
}


static PyObject * xsbp_queryAll(PyObject *self, PyObject *args)
{
	char *cmd;
	if(!PyArg_ParseTuple(args, "s", &cmd))
		return NULL;
	printf("%s", cmd);
	int rcp;
	XSB_StrDefine(p_return_string);
	
	xsb_query_save(3);
	rcp = xsb_query_string_string(cmd,&p_return_string,"|");
	printf("here2");
	while (rcp == XSB_SUCCESS ) {
	 	printf("Return p %s\n",(p_return_string.string));
	 	rcp = xsb_next_string(&p_return_string,"|");
	 }
	xsb_query_restore();
	return Py_BuildValue("s", p_return_string.string);
}
